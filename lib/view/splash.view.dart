import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:login_signup/view/identity/sign.In.dart';
import 'package:login_signup/view/identity/sign.up.dart';

class SplashView extends StatefulWidget {
  const SplashView({Key? key}) : super(key: key);

  @override
  State<SplashView> createState() => _SplashView();

}



class _SplashView extends State<SplashView> {
  @override
  Widget build(BuildContext context) {

    Timer(const Duration(seconds: 3), () {

      Navigator.of(context).push(
          MaterialPageRoute(builder: (context) => const SignUp()));

    });

    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
          child: Image.asset(
        'assets/images/logo.png',
        width: 150,
        height: 150,
      )),
    );
  }
}
