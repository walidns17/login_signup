import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:login_signup/%20utils/global.colors.dart';
import 'package:login_signup/view/identity/sign.In.dart';
import 'package:login_signup/widgets/button.global.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: const EdgeInsets.only(left: 15, right: 15, top: 50),
        child: SingleChildScrollView(

          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [

              Container(
                width: 55,
                height: 55,
                decoration: BoxDecoration(
                  color: Colors.grey[200],
                  borderRadius: BorderRadius.circular(15),
                ),
                child: const Center(
                    child: Icon(Icons.close_outlined),
                )
              ),
              const SizedBox(
                height: 20,
              ),
              const Row(
                children: [
                  Text(
                    "Hey ",
                    style: TextStyle(
                      color: Colors.deepOrange,
                      fontSize: 22,
                    ),
                  ),
                  Text(
                    "Champ",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 22,
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              const Row(
                children: [
                  Text(
                    "welcome back to ",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                    ),
                  ),
                  Text(
                    "WafraAlgeria.com",
                    style: TextStyle(
                      color: Colors.deepOrange,
                      fontSize: 18,
                    ),
                  ),
                ],
              ),

              const SizedBox(
                height: 10,
              ),
              Container(
                alignment: Alignment.topLeft,
                child: const Text(
                  "Crowd is waiting for you...",
                  style: TextStyle(
                    color: Colors.black54,
                    fontSize: 16,
                  ),
                ),
              ),

              const SizedBox(
                height: 40,
              ),

              /////////////////*************

              Container(
                alignment: Alignment.topLeft,
                margin: const EdgeInsets.fromLTRB(14, 0, 0, 0),
                child: const Text(
                  "Enter your email id",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                  ),
                ),
              ),

              const SizedBox(
                height: 10,
              ),
              Container(
                height: 55,
                width: MediaQuery.of(context).size.width / 1.12,
                decoration: BoxDecoration(
                  color: Colors.grey[200],
                  borderRadius: BorderRadius.circular(15),
                ),
                child: TextFormField(
                  // controller: cardHolderNameController,
                  keyboardType: TextInputType.emailAddress,
                  decoration: const InputDecoration(
                    border: InputBorder.none,
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                    hintText: 'test@example.com',
                    hintStyle: TextStyle(
                      color: Colors.grey,
                      fontSize: 16,
                    ),
                    prefixIcon: Icon(
                      Icons.email,
                      color: Colors.grey,
                    ),
                  ),
                  onChanged: (value) {
                    setState(() {
                      // cardHolderNameController.value =
                      //     cardHolderNameController.value.copyWith(
                      //         text: value,
                      //         selection:
                      //         TextSelection.collapsed(offset: value.length),
                      //         composing: TextRange.empty);
                    });
                  },
                ),
              ),

              /////////////////*************

              const SizedBox(
                height: 20,
              ),
              Container(
                alignment: Alignment.topLeft,
                margin: const EdgeInsets.fromLTRB(14, 0, 0, 0),
                child: const Text(

                  "Set password",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Container(
                height: 55,
                width: MediaQuery.of(context).size.width / 1.12,
                decoration: BoxDecoration(
                  color: Colors.grey[200],
                  borderRadius: BorderRadius.circular(15),
                ),
                child: TextFormField(
                  // controller: cardHolderNameController,
                  keyboardType: TextInputType.visiblePassword,
                  decoration: const InputDecoration(
                    border: InputBorder.none,
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                    hintText: 'check your caps-lock',
                    hintStyle: TextStyle(
                      color: Colors.grey,
                      fontSize: 16,
                    ),
                    prefixIcon: Icon(
                      Icons.password,
                      color: Colors.grey,
                    ),
                  ),
                  onChanged: (value) {
                    setState(() {
                      // cardHolderNameController.value =
                      //     cardHolderNameController.value.copyWith(
                      //         text: value,
                      //         selection:
                      //         TextSelection.collapsed(offset: value.length),
                      //         composing: TextRange.empty);
                    });
                  },
                ),
              ),
              /////////////////*************

              /////////////////*************

              const SizedBox(
                height: 20,
              ),
              Container(
                alignment: Alignment.topLeft,
                margin: const EdgeInsets.fromLTRB(14, 0, 0, 0),
                child: const Text(

                  "Set password",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Container(
                height: 55,
                width: MediaQuery.of(context).size.width / 1.12,
                decoration: BoxDecoration(
                  color: Colors.grey[200],
                  borderRadius: BorderRadius.circular(15),
                ),
                child: TextFormField(
                  // controller: cardHolderNameController,
                  keyboardType: TextInputType.visiblePassword,
                  decoration: const InputDecoration(
                    border: InputBorder.none,
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                    hintText: 'check your caps-lock',
                    hintStyle: TextStyle(
                      color: Colors.grey,
                      fontSize: 16,
                    ),
                    prefixIcon: Icon(
                      Icons.password,
                      color: Colors.grey,
                    ),
                  ),
                  onChanged: (value) {
                    setState(() {
                      // cardHolderNameController.value =
                      //     cardHolderNameController.value.copyWith(
                      //         text: value,
                      //         selection:
                      //         TextSelection.collapsed(offset: value.length),
                      //         composing: TextRange.empty);
                    });
                  },
                ),
              ),

              /////////////////*************

              const SizedBox(
                height: 20,
              ),
              SizedBox(
                width: double.infinity,
                height: 45,
                child: ButtonGlobal(
                  onPress: (){},
                  text: "Sign Up",
                ),
              ),
              const SizedBox(
                height: 20,
              ),

              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    "Already have an account ?",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                    ),
                  ),
                  InkWell(
                    onTap: () {_signIn();} ,
                    child: const Text(
                      " Sign In",
                      style: TextStyle(
                        color: Colors.deepOrange,
                        fontSize: 18,
                      ),
                    ),
                  ),
                ],
              ),

              const SizedBox(
                height: 20,
              ),

              Container(
                alignment: Alignment.center,
                child: const Text(
                  "Or Sign In With",
                  style: TextStyle(
                    color: Colors.black38,
                    fontSize: 16,
                  ),
                ),
              ),

              const SizedBox(
                height: 40,
              ),

              Row(
                children: [
                  Container(
                    height: 55,
                    width: MediaQuery.of(context).size.width / 2.2,
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: Center(
                        child: Image.asset(
                      "assets/icons/google_ic.png",
                      width: 25,
                      height: 25,
                    )),
                  ),
                  const Spacer(),
                  Container(
                    height: 55,
                    width: MediaQuery.of(context).size.width / 2.2,
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: Center(
                        child: Image.asset(
                      "assets/icons/facebook_ic.png",
                      width: 25,
                      height: 25,
                    )),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  void _signIn() async {
    Navigator.of(context).push(
        MaterialPageRoute(builder: (context) => const SignIn()));
  }

}
