import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:login_signup/view/identity/sign.up.dart';
import 'package:login_signup/widgets/button.global.dart';

class SignIn extends StatefulWidget {
  const SignIn({Key? key}) : super(key: key);

  @override
  State<SignIn> createState() => _SignInState();
}

class _SignInState extends State<SignIn> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: const EdgeInsets.only(left: 15, right: 15, top: 50),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [

              Container(
                  width: 55,
                  height: 55,
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: const Center(
                    child: Icon(Icons.close_outlined),
                  )
              ),
              const SizedBox(
                height: 20,
              ),
              const Row(
                children: [
                  Text(
                    "Hey ",
                    style: TextStyle(
                      color: Colors.deepOrange,
                      fontSize: 22,
                    ),
                  ),
                  Text(
                    "Champ",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 22,
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              const Row(
                children: [
                  Text(
                    "welcome back to ",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                    ),
                  ),
                  Text(
                    "WafraAlgeria.com",
                    style: TextStyle(
                      color: Colors.deepOrange,
                      fontSize: 18,
                    ),
                  ),
                ],
              ),

              const SizedBox(
                height: 10,
              ),
              Container(
                alignment: Alignment.topLeft,
                child: const Text(
                  "Crowd is waiting for you...",
                  style: TextStyle(
                    color: Colors.black54,
                    fontSize: 16,
                  ),
                ),
              ),

              const SizedBox(
                height: 70,
              ),

              /////////////////*************

              Container(
                alignment: Alignment.topLeft,
                margin: const EdgeInsets.fromLTRB(15, 0, 0, 0),

                child: const Text(
                  "Enter your registered email id",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                  ),
                ),
              ),

              const SizedBox(
                height: 10,
              ),
              Container(
                height: 55,
                width: MediaQuery.of(context).size.width / 1.12,
                decoration: BoxDecoration(
                  color: Colors.grey[200],
                  borderRadius: BorderRadius.circular(15),
                ),
                child: TextFormField(
                  // controller: cardHolderNameController,
                  keyboardType: TextInputType.emailAddress,
                  decoration: const InputDecoration(
                    border: InputBorder.none,
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                    hintText: 'test@example.com',
                    hintStyle: TextStyle(
                      color: Colors.grey,
                      fontSize: 16,
                    ),
                    prefixIcon: Icon(
                      Icons.email,
                      color: Colors.grey,
                    ),
                  ),
                  onChanged: (value) {
                    setState(() {

                    });
                  },
                ),
              ),

              /////////////////*************

              const SizedBox(
                height: 20,
              ),
              Container(
                alignment: Alignment.topLeft,
                margin: const EdgeInsets.fromLTRB(15, 0, 0, 0),

                child: const Text(
                  "Enter your password",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),

              Container(
                height: 55,
                width: MediaQuery.of(context).size.width / 1.12,
                decoration: BoxDecoration(
                  color: Colors.grey[200],
                  borderRadius: BorderRadius.circular(15),
                ),
                child: TextFormField(
                  // controller: cardHolderNameController,
                  keyboardType: TextInputType.visiblePassword,
                  decoration: const InputDecoration(
                    border: InputBorder.none,
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                    hintText: 'check your caps-lock',
                    hintStyle: TextStyle(
                      color: Colors.grey,
                      fontSize: 16,
                    ),
                    prefixIcon: Icon(
                      Icons.password,
                      color: Colors.grey,
                    ),
                  ),
                  onChanged: (value) {
                    setState(() {

                    });
                  },
                ),
              ),
              /////////////////*************

              Container(
                alignment: Alignment.centerRight,
                margin: const EdgeInsets.all(15),
                child: const Text(
                  "Having issues? Reset password new",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 14,
                  ),
                  textAlign: TextAlign.end,
                ),
              ),

              const SizedBox(
                height: 20,
              ),
              SizedBox(
                width: double.infinity,
                height: 45,
                child: ButtonGlobal(
                  onPress:() {},
                  text: "Sign In",
                ),
              ),
              const SizedBox(
                height: 20,
              ),

               Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    "Don't have an account ?",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                    ),
                  ),
                  InkWell(
                    onTap: () {_signUp();} ,
                    child: const Text(
                      " Create New",
                      style: TextStyle(
                        color: Colors.deepOrange,
                        fontSize: 18,
                      ),

                    ),
                  ),
                ],
              ),

              const SizedBox(
                height: 20,
              ),

              Container(
                alignment: Alignment.center,
                child: const Text(
                  "Or Sign In With",
                  style: TextStyle(
                    color: Colors.black38,
                    fontSize: 16,
                  ),
                ),
              ),

              const SizedBox(
                height: 40,
              ),

              Row(
                children: [
                  Container(
                    height: 55,
                    width: MediaQuery.of(context).size.width / 2.2,
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: Center(
                        child: Image.asset(
                      "assets/icons/google_ic.png",
                      width: 25,
                      height: 25,
                    )),
                  ),
                  const Spacer(),
                  Container(
                    height: 55,
                    width: MediaQuery.of(context).size.width / 2.2,
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: Center(
                        child: Image.asset(
                      "assets/icons/facebook_ic.png",
                      width: 25,
                      height: 25,
                    )),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  void _signUp() async {
    Navigator.of(context).push(
        MaterialPageRoute(builder: (context) => const SignUp()));
  }

}
